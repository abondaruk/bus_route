package com.goeuro.challenge.bus_route.providers;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class BusRouteProviderImpl implements BusRouteProvider {

	private int[] routes = null;
	private Map<Integer, BitSet> busStops = new HashMap<>();

	public void loadFromFile(File file) {

		LineIterator it = null;
		try {
			it = FileUtils.lineIterator(file, "UTF-8");

			final int routeCount = it.hasNext() ? Integer.parseInt(it.nextLine()) : 0;
			if (routeCount == 0)
				throw new IllegalArgumentException("Invalid file");

			routes = new int[routeCount];
			int lineNo = 0;
			while (it.hasNext()) {
				String[] lineItems = it.nextLine().split(" ");

				routes[lineNo] = Integer.parseInt(lineItems[0]);
				for (int i = 1; i < lineItems.length; i++) {

					Integer busStop = Integer.parseInt(lineItems[i]);
					busStops
						.computeIfAbsent(busStop, v -> new BitSet(routeCount))
						.set(lineNo);
				}

				lineNo++;
			}
		}
		catch (IllegalArgumentException | IOException e) {

			log.error("Cannot load data from file", e);
		}
		finally {
			LineIterator.closeQuietly(it);
		}
	}

	public boolean checkDirectConnection(Integer departureId, Integer arrivalId) {

		BitSet busStopRoutes = busStops.getOrDefault(departureId, null);
		if (busStopRoutes == null)
			return false;

		BitSet busStopRoutes2 = busStops.getOrDefault(arrivalId, null);
		if (busStopRoutes2 == null)
			return false;

		return busStopRoutes.intersects(busStopRoutes2);
	}
}
