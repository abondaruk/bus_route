package com.goeuro.challenge.bus_route.providers;

public interface BusRouteProvider {

	boolean checkDirectConnection(Integer departureId, Integer arrivalId);
}
