package com.goeuro.challenge.bus_route.controllers;

import com.goeuro.challenge.bus_route.dtos.BusRouteDto;
import com.goeuro.challenge.bus_route.services.BusRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/")
public class BusRouteController {

	@Autowired
	private BusRouteService busRouteService;

	@GetMapping("direct")
	public HttpEntity<BusRouteDto> getkDirectConnection(
		@RequestParam(name = "dep_sid", required = true) Integer departureId,
		@RequestParam(name = "arr_sid", required = true) Integer arrivalId) {

		return new HttpEntity<>(busRouteService.checkDirectConnection(departureId, arrivalId));
	}
}
