package com.goeuro.challenge.bus_route.services;

import com.goeuro.challenge.bus_route.dtos.BusRouteDto;
import com.goeuro.challenge.bus_route.providers.BusRouteProvider;
import com.goeuro.challenge.bus_route.providers.BusRouteProviderImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class BusRouteService {

	@Autowired
	private Environment env;

	private BusRouteProvider busRouteProvider;

	public BusRouteDto checkDirectConnection(Integer departureId, Integer arrivalId) {

		checkInitialized();
		return BusRouteDto.builder()
			.dep_sid(departureId)
			.arr_sid(arrivalId)
			.direct_bus_route(busRouteProvider.checkDirectConnection(departureId, arrivalId))
			.build();
	}

	private void checkInitialized() {

		if (busRouteProvider == null)
			initialize();
	}

	private synchronized void initialize() {

		if (busRouteProvider != null)
			return;

		BusRouteProviderImpl provider = new BusRouteProviderImpl();
		provider.loadFromFile(new File(env.getRequiredProperty("dataFile")));
		busRouteProvider = provider;
	}
}
