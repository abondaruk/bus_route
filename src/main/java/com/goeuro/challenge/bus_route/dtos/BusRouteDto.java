package com.goeuro.challenge.bus_route.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BusRouteDto {
	Integer dep_sid;
	Integer arr_sid;
	Boolean direct_bus_route;
}
