package com.goeuro.challenge.bus_route.services;

import com.goeuro.challenge.bus_route.BusRouteApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BusRouteServiceTest extends BusRouteApplicationTests {

	@Autowired
	BusRouteService busRouteService;

	@Test
	void test1() throws IOException {

		assertTrue(busRouteService.checkDirectConnection(153, 150).getDirect_bus_route());
		assertFalse(busRouteService.checkDirectConnection(153, 142).getDirect_bus_route());
	}
}